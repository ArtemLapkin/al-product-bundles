<?php

/**
 * The admin-specific functionality of the plugin.
 *
 * @link       https://www.linkedin.com/in/artemlapkin/
 * @since      1.0.0
 *
 * @package    Al_Product_Bundles
 * @subpackage Al_Product_Bundles/admin
 */

/**
 * The admin-specific functionality of the plugin.
 *
 * Defines the plugin name, version, and two examples hooks for how to
 * enqueue the admin-specific stylesheet and JavaScript.
 *
 * @package    Al_Product_Bundles
 * @subpackage Al_Product_Bundles/admin
 * @author     Artem Lapkin <lapkin.artem@gmail.com>
 */
class Al_Product_Bundles_Admin {

	/**
	 * The ID of this plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      string    $plugin_name    The ID of this plugin.
	 */
	private $plugin_name;

	/**
	 * The version of this plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      string    $version    The current version of this plugin.
	 */
	private $version;

	/**
	 * Initialize the class and set its properties.
	 *
	 * @since    1.0.0
	 * @param      string    $plugin_name       The name of this plugin.
	 * @param      string    $version    The version of this plugin.
	 */
	public function __construct( $plugin_name, $version ) {

		$this->plugin_name = $plugin_name;
		$this->version = $version;

	}

	/**
	 * Register the stylesheets for the admin area.
	 *
	 * @since    1.0.0
	 */
	public function enqueue_styles() {

		/**
		 * This function is provided for demonstration purposes only.
		 *
		 * An instance of this class should be passed to the run() function
		 * defined in Al_Product_Bundles_Loader as all of the hooks are defined
		 * in that particular class.
		 *
		 * The Al_Product_Bundles_Loader will then create the relationship
		 * between the defined hooks and the functions defined in this
		 * class.
		 */

		wp_enqueue_style( $this->plugin_name, plugin_dir_url( __FILE__ ) . 'css/al-product-bundles-admin.css', array(), $this->version, 'all' );

	}

	/**
	 * Register the JavaScript for the admin area.
	 *
	 * @since    1.0.0
	 */
	public function enqueue_scripts() {

		/**
		 * This function is provided for demonstration purposes only.
		 *
		 * An instance of this class should be passed to the run() function
		 * defined in Al_Product_Bundles_Loader as all of the hooks are defined
		 * in that particular class.
		 *
		 * The Al_Product_Bundles_Loader will then create the relationship
		 * between the defined hooks and the functions defined in this
		 * class.
		 */

		wp_enqueue_script( $this->plugin_name, plugin_dir_url( __FILE__ ) . 'js/al-product-bundles-admin.js', array( 'jquery' ), $this->version, false );

	}

    /**
     * Prints order meta
     * @param $order
     */
    public function print_order_meta($order) {

        $logo_data = get_post_meta($order->get_id(), 'alpb_logo', true);
        $bundles_counts = $this->get_bundles_couns($order->get_id());

        if( empty( $logo_data ) ) return;

        echo '<h3>' . __('Logo Print Details', 'al-product-bundles') . ':</h3><br>';

        foreach ($logo_data as $bciid => $logo_datum) {
            $bundle_title = isset( $bundles_counts[$bciid] ) ? $bundles_counts[$bciid] : '';
            $logo_link = isset( $logo_datum['logo_data']['logo_url'] ) ? $logo_datum['logo_data']['logo_url'] : '';
            $product_with_logo = isset( $logo_datum['products_with_logo'] ) ? $logo_datum['products_with_logo'] : array();

            if($bundle_title !== '') {
                echo '<span><strong>' . $bundle_title . '</strong></span><br>';
            }
            if($logo_link !== '') {
                echo '<span> - <a href="' . $logo_link . '" target="_blank">' . __('logo', 'al-product-bundles') . '</a></span><br>';
            }
            if( !empty( $product_with_logo )) {
                echo '<span>' . __('Products with logo print ', 'al-product-bundles') . ':</span><br>';
                foreach ($product_with_logo as $product_with_logo) {
                    echo '<span> - ' . $product_with_logo . '</span><br>';
                }
            }
            echo '<br>';
        }

	}

    public function view_order_line_item_meta($item_id, $item) {
        global $post;

        $item_logo = $item->get_meta('alpb_logo');
        $bciid = $item->get_meta('alpb_bciid');

        $bundles_counts = $this->get_bundles_couns($post->ID);

        // added product, print to what bundle its related
        if( $bciid !== '' ) {
            $bundle_related_to =  isset( $bundles_counts[$bciid] ) ? $bundles_counts[$bciid] : '';
            if( $bundle_related_to !== '' ) {
                echo '<p>' . __('added to bundle', 'al-product-bundles') . ' - <strong>' . $bundle_related_to . '</strong></p>';
            }
        }

        // logo product - print to what data its related
        if(  $item_logo !== '' && isset( $item_logo['bciid'] ) ) {

            $bundle_related_to =  isset( $bundles_counts[$item_logo['bciid']] ) ? $bundles_counts[$item_logo['bciid']] : '';
            
            if( $bundle_related_to !== '' ) {
                echo '<p>' . __('added to bundle', 'al-product-bundles') . '- ' . $bundle_related_to . '</p>';
            }
        }

        $is_removed = $this->is_standard_product_removed($item);
        if( $is_removed ) {
            echo '<p>' . __('Was Removed', 'al-product-bundles') . '</p>';
        }
	}


	public function delete_order_hook($id) {


        global $post_type;

        if($post_type !== 'shop_order') {
            return;
        }

        $meta = get_post_meta($id, 'alpb_logo', true);

        if( empty( $meta ) ) return;


        $first_bundle = array_shift( $meta );

        
        if( isset( $first_bundle['logo_path'] ) && file_exists($first_bundle['logo_path'])) {
            unlink($first_bundle['logo_path']);
        }

    }

    public function update_product_title($title, $item) {
        
        global $post;
        
        $alpb_bundle_hash = $item->get_meta('alpb_bundle_hash');

        if( $alpb_bundle_hash !== '' ) {

            $bundles_counts = $this->get_bundles_couns($post->ID);

            if( empty( $bundles_counts ) ) return $title;

            $title = isset( $bundles_counts[$alpb_bundle_hash] ) ? $bundles_counts[$alpb_bundle_hash] : $title;
            
        }
        return $title;
    }

    /**
     * Returns bundle names witho counter
     * @param $order_id
     * @return mixed
     */
    private function get_bundles_couns( $order_id ) {
        return get_post_meta($order_id, 'alpb_bundles_counts', true);
    }


    private function get_bundles_hidden_standard_items($order_id) {
        return get_post_meta($order_id, 'alpb_removed_standard_items', true);
    }

    /**
     * Prints 'bundled' class for items related to bundle
     * @param $class
     * @param $item
     * @return string
     */
    public function woocommerce_admin_html_order_item_class($class, $item) {
        $item_logo = $item->get_meta('alpb_logo');
        $bciid = $item->get_meta('alpb_bciid');
        
        if( $bciid !=='' || ( isset( $item_logo['bciid'] ) && $item_logo['bciid'] !== '' ) ) {
            $class .= ' yith-wcpb-admin-bundled-item';
        }

        $is_removed = $this->is_standard_product_removed($item);
        if( $is_removed ) {
            $class .= ' al-bg-removed';
        }

        return $class;
    }

    /**
     * Hides item meta on edit order page,
     * this meta is needed only for logic purposes, but not for view
     */
    public function woocommerce_order_item_get_formatted_meta_data($meta_data, $item) {

        foreach ($meta_data as $key => $meta_datum) {
            if( $meta_datum->key === 'alpb_bciid'
                || $meta_datum->key === 'alpb_bundle_hash'
                || $meta_datum->key === 'alpb_added_products'
                || $meta_datum->key === 'alpb_bundle_variations'
                || $meta_datum->key === 'alpb_standard_items_hidden'
                || $meta_datum->key === 'alpb_bundled_item_id') {
                unset($meta_data[$key]);
            }
        }
        return $meta_data;
    }


    /**
     * Checks if standard product was removed
     * @param $item
     * @return bool
     */
    private function is_standard_product_removed($item) {
        $out = false;
        $bundled_by = $item->get_meta('_bundled_by');
        if( $bundled_by !== '' ) {
            $order_id = isset( $_GET['post'] ) ? $_GET['post'] : false;
            $bundled_item_it = $item->get_meta('alpb_bundled_item_id');

            $hidden_items = $this->get_bundles_hidden_standard_items($order_id);
            if( is_array( $hidden_items ) && isset( $hidden_items[$bundled_by] ) && is_array( $hidden_items[$bundled_by] ) && in_array($bundled_item_it, $hidden_items[$bundled_by]) ) {
                $out = true;
            }
        }

        return $out;
    }

    /**
     * Prints field on edit product page
     */
    public function field_setting_remove_standard_products() {
        $prod_id = $_GET['post'];
        $product   = wc_get_product( $prod_id );
        if ( $product->get_type() === 'yith_bundle' ) {
        ?>
        <div class="options_group reviews">
            <?php
            woocommerce_wp_checkbox(
                array(
                    'id'      => 'al_cant_remove_standard_prods',
                    'value'   => Al_Product_Bundles::get_product_setting_remove_standard_product($prod_id) === 'yes' ? 'yes' : '',
                    'label'   => __( 'Standard Products are not removable', 'woocommerce' ),
                    'cbvalue' => 'yes',
                )
            );
            ?>
        </div>
        <?php
        }
    }

    /**
     * Saves 'Cant remove standard products' field
     * @param $prod_id
     */
    public function store_product_data($prod_id) {
        if( ! isset( $_POST['al_cant_remove_standard_prods'] ) ) {
            update_post_meta( $prod_id, 'al_cant_remove_standard_prods', '' );
        }

        $current_val = get_post_meta($prod_id, 'al_cant_remove_standard_prods');

        if( $current_val === Al_Product_Bundles::get_product_setting_remove_standard_product($prod_id) ) return;

        if( $_POST['al_cant_remove_standard_prods'] === 'yes' ) {
            update_post_meta( $prod_id, 'al_cant_remove_standard_prods', 'yes' );
        }
    }
}
