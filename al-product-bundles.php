<?php

/**
 * The plugin bootstrap file
 *
 * This file is read by WordPress to generate the plugin information in the plugin
 * admin area. This file also includes all of the dependencies used by the plugin,
 * registers the activation and deactivation functions, and defines a function
 * that starts the plugin.
 *
 * @link              https://www.linkedin.com/in/artemlapkin/
 * @since             1.0.0
 * @package           Al_Product_Bundles
 *
 * @wordpress-plugin
 * Plugin Name:       Al Product Bundles
 * Plugin URI:         
 * Description:       Product Bundles on frontend. Removes standard products, has logo print, calculated cart item price correctly
 * Version:           1.0.0
 * Author:            Artem Lapkin
 * Author URI:        https://www.linkedin.com/in/artemlapkin/
 * License:           GPL-2.0+
 * License URI:       http://www.gnu.org/licenses/gpl-2.0.txt
 * Text Domain:       al-product-bundles
 * Domain Path:       /languages
 */

// If this file is called directly, abort.
if ( ! defined( 'WPINC' ) ) {
	die;
}

/**
 * Currently plugin version.
 * Start at version 1.0.0 and use SemVer - https://semver.org
 * Rename this for your plugin and update it as you release new versions.
 */
define( 'AL_PRODUCT_BUNDLES', '1.0.0' );

/**
 * The code that runs during plugin activation.
 * This action is documented in includes/class-al-product-bundles-activator.php
 */
function activate_al_product_bundles() {
	require_once plugin_dir_path( __FILE__ ) . 'includes/class-al-product-bundles-activator.php';
	Al_Product_Bundles_Activator::activate();
}

/**
 * The code that runs during plugin deactivation.
 * This action is documented in includes/class-al-product-bundles-deactivator.php
 */
function deactivate_al_product_bundles() {
	require_once plugin_dir_path( __FILE__ ) . 'includes/class-al-product-bundles-deactivator.php';
	Al_Product_Bundles_Deactivator::deactivate();
}

register_activation_hook( __FILE__, 'activate_al_product_bundles' );
register_deactivation_hook( __FILE__, 'deactivate_al_product_bundles' );

/**
 * The core plugin class that is used to define internationalization,
 * admin-specific hooks, and public-facing site hooks.
 */
require plugin_dir_path( __FILE__ ) . 'includes/class-al-product-bundles.php';

/**
 * Begins execution of the plugin.
 *
 * Since everything within the plugin is registered via hooks,
 * then kicking off the plugin from this point in the file does
 * not affect the page life cycle.
 *
 * @since    1.0.0
 */
function run_al_product_bundles() {

    if( ! al_product_bundles_needed_plugins() ) {

        add_action('admin_notices', 'al_product_bundles_admin_notice');

        return;
    }

	$plugin = new Al_Product_Bundles();
	$plugin->run();

}
run_al_product_bundles();

/**
 * Check if needed plugins are active
 * @return bool
 */
function al_product_bundles_needed_plugins(){

    if( ! function_exists('is_plugin_active') ) {
        include_once(ABSPATH . 'wp-admin/includes/plugin.php');
    }

    $flag = ! is_plugin_active('yith-woocommerce-product-bundles-premium/init.php') || ! is_plugin_active('woocommerce/woocommerce.php') ? false : true;

    return $flag;
}

/**
 * Print notice if plugin active but doesn't work
 */
function al_product_bundles_admin_notice() {
    ?>
    <div class="error">
        <p><?php _e( 'Al Product Bundles is enabled but not effective. It requires WooCommerce & YITH WooCommerce Product Bundles Premium in order to work.', 'al-product-bundles' ); ?></p>
    </div>
    <?php
}