=== Plugin Name ===
Donate link: https://www.linkedin.com/in/artemlapkin/
Tags: woocommerce
Requires at least: 3.0.1
Tested up to: 3.4
Stable tag: 4.3
License: GPLv2 or later
License URI: http://www.gnu.org/licenses/gpl-2.0.html

Functional extension for YITH WooCommerce Product Bundles

== Description ==

User can add products to bundle on front-end, upload logo he wants to print on products, select products that need logo print.

== Installation ==

1. Upload `al-product-bundles.php` to the `/wp-content/plugins/` directory
2. Activate the plugin through the 'Plugins' menu in WordPress
