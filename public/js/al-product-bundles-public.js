(function( $ ) {
	'use strict';
	$(document).ready(function (){

	    if( $($('#al-product-bundles-modal')).length > 0 ) {
            // Ajax remove prod from session
            $('.al-js-remove').click(function (e){
                e.preventDefault();
                var $this = $(this),
                    prodId = $this.data('product-id'),
                    parentLi = $this.parents('li'),
                    loader = $('.al-js-loader'),
                    bundleContentWrap = $('.al-js-bundle-inner'),
                    isStandard = $this.data('standard-product');

                if( ! Number.isInteger(prodId) ) {
                    return;
                }

                $this.attr('disabled', 'disabled');
                loader.addClass('active');

                var data = {
                    action: 'alpb_remove_from_bundle',
                    'product-id': prodId,
                    'is-standard' : isStandard
                };

                $.ajax( {
                    type   : "POST",
                    data   : data,
                    url    : ajax_obj.ajaxurl,
                    success: function ( response ) {
                        var resp = JSON.parse(response);
                        if( resp.status ) {
                            if( isStandard ) {
                                parentLi.addClass('al-hidden');
                                var restoreButton = $('.al-js-restore-stand-prod ');
                                if( restoreButton.hasClass('al-hidden') ) {
                                    restoreButton.removeClass('al-hidden');
                                }
                                if( resp.last_standard_product_removed ) {
                                    $('.al-js-hidden-if-no-products').hide();
                                }
                            } else {
                                parentLi.remove();
                            }
                            bundleContentWrap.html(resp.bundle_content)
                        } else {
                            alert(resp.msg);
                        }
                        $this.attr('disabled', false);
                        loader.removeClass('active');
                    }
                } );
            });

            // Ajax load second step in modal
            $('body').on('click', '.al-js-modal-second-step', function (){
                var loader = $('.al-js-loader'),
                    step = $(this).data('step');

                // Show modal start page
                if( step === 0 ) {
                    alShowStepOne();
                    return;
                }

                var data = {
                    action: 'alpb_modal_step',
                    step: step
                };
                
                loader.addClass('active');

                $.ajax( {
                    type   : "POST",
                    data   : data,
                    url    : ajax_obj.ajaxurl,
                    success: function ( response ) {
                        var resp = JSON.parse(response);
                        if( resp.status ) {
                            $('.al-js-first-step').hide();
                            $('.al-js-second-step').html(resp.body)
                        } else {
                            console.log(resp.msg);
                            window.location = window.location.href;
                        }
                        loader.removeClass('active');
                    }
                } );
            });

            // mark li in modal second step
            $('body').on('click', '.al-js-mark', function (){
                var $this = $(this),
                    prodId = $this.data('product-id'),
                    hidInp = $('.al-js-logo-products'),
                    addedProd = hidInp.val();

                if( ! Number.isInteger(prodId)) {
                    return;
                }
                var addedProdArr = addedProd !== '' ? JSON.parse(addedProd) : []

                $this.toggleClass('marked');

                if( $this.hasClass('marked') && $.inArray(prodId,addedProdArr) === -1 ) {
                    // add product ID
                    addedProdArr.push(prodId);

                }else if( addedProdArr.length > 0 && $.inArray(prodId,addedProdArr) ) {
                    // remove product ID
                    addedProdArr.splice($.inArray(prodId,addedProdArr), 1);
                }
                hidInp.val(JSON.stringify(addedProdArr));
            });

            // on modal close, remove step 2, show step 1
            $('#al-product-bundles-modal').on('hidden.bs.modal', function () {
                alShowStepOne();
            });

            // opens modal on checkout page, loads content to it
            $('body').on('click', '.al-js-modal-in-cart', function (e){
                e.preventDefault();

                var $this = $(this),
                    prodID = $this.data('product-id'),
                    addedProd = $this.data('added-products'),
                    modal = $('#al-product-bundles-modal'),
                    container = modal.find('.modal-content');

                if( prodID === '' ) {
                    return false;
                }

                $('.woocommerce-cart-form').block( {
                    message: null,
                    overlayCSS: {
                        background: '#fff',
                        opacity: 0.6
                    }
                } );

                var data = {
                    action: 'alpb_get_modal_content',
                    product_id : prodID,
                    added_products: addedProd
                };

                $.ajax( {
                    type   : "POST",
                    data   : data,
                    url    : ajax_obj.ajaxurl,
                    success: function ( response ) {
                        var resp = JSON.parse(response);
                        if( resp.status ) {
                            container.html(resp.body)
                        } else {
                            console.log(resp.msg);
                        }
                        modal.modal('show');
                        $('.woocommerce-cart-form').unblock();
                    }
                } );
            });
        }

        // restore removed standard products
        $('body').on('click', '.al-js-restore-stand-prod', function (){
            var loader = $('.al-js-loader'),
                bundleContentWrap = $('.al-js-bundle-inner'),
                data = {
                    action: 'alpb_restore_standard_products'
                };
            loader.addClass('active');

            $.ajax( {
                type   : "POST",
                data   : data,
                url    : ajax_obj.ajaxurl,
                success: function ( response ) {
                    var resp = JSON.parse(response);
                    if( resp.status ) {
                        $('.al-standard-product').removeClass('al-hidden');
                        bundleContentWrap.html(resp.bundle_content);
                        $('.al-js-hidden-if-no-products').show();

                    } else {
                        console.log(resp.msg);
                    }
                    loader.removeClass('active');
                }
            } );
        });

        // show step one in modal
        function alShowStepOne() {
            $('.al-js-first-step').show();
            $('.al-js-second-step').html('');
        }

        $(document).on('click', '#al-product-bundles-modal .al-js-set-qty', function () {
            var $this = $(this),
                qtyInp = $this.parents('.form-group').find('input[type="number"]'),
                currentVal = qtyInp.val() * 1,
                minVal =  qtyInp.attr('min');

            if( $this.hasClass('al-plus') ) {
                qtyInp.val(currentVal + 1 );
            } else if( currentVal > minVal ) {
                qtyInp.val(currentVal - 1 );
            }
        });

	});
})( jQuery );
