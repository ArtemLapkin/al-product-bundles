<?php

/**
 * The public-facing functionality of the plugin.
 *
 * @link       https://www.linkedin.com/in/artemlapkin/
 * @since      1.0.0
 *
 * @package    Al_Product_Bundles
 * @subpackage Al_Product_Bundles/public
 */

/**
 * The public-facing functionality of the plugin.
 *
 * Defines the plugin name, version, and two examples hooks for how to
 * enqueue the public-facing stylesheet and JavaScript.
 *
 * @package    Al_Product_Bundles
 * @subpackage Al_Product_Bundles/public
 * @author     Artem Lapkin <lapkin.artem@gmail.com>
 */
class Al_Product_Bundles_Public {

	/**
	 * The ID of this plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      string    $plugin_name    The ID of this plugin.
	 */
	private $plugin_name;

	/**
	 * The version of this plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      string    $version    The current version of this plugin.
	 */
	private $version;

    /**
     * @var
     */
	private $wrapping_price;

	/**
	 * Initialize the class and set its properties.
	 *
	 * @since    1.0.0
	 * @param      string    $plugin_name       The name of the plugin.
	 * @param      string    $version    The version of this plugin.
	 */
	public function __construct( $plugin_name, $version ) {

		$this->plugin_name = $plugin_name;
		$this->version = $version;

		$this->wrapping_price = 0.25;

	}

	/**
	 * Register the stylesheets for the public-facing side of the site.
	 *
	 * @since    1.0.0
	 */
	public function enqueue_styles() {

		/**
		 * This function is provided for demonstration purposes only.
		 *
		 * An instance of this class should be passed to the run() function
		 * defined in Al_Product_Bundles_Loader as all of the hooks are defined
		 * in that particular class.
		 *
		 * The Al_Product_Bundles_Loader will then create the relationship
		 * between the defined hooks and the functions defined in this
		 * class.
		 */

		wp_enqueue_style( $this->plugin_name, plugin_dir_url( __FILE__ ) . 'css/al-product-bundles-public.css', array(), $this->version, 'all' );

	}

	/**
	 * Register the JavaScript for the public-facing side of the site.
	 *
	 * @since    1.0.0
	 */
	public function enqueue_scripts() {

		/**
		 * This function is provided for demonstration purposes only.
		 *
		 * An instance of this class should be passed to the run() function
		 * defined in Al_Product_Bundles_Loader as all of the hooks are defined
		 * in that particular class.
		 *
		 * The Al_Product_Bundles_Loader will then create the relationship
		 * between the defined hooks and the functions defined in this
		 * class.
		 */

		wp_enqueue_script( $this->plugin_name, plugin_dir_url( __FILE__ ) . 'js/al-product-bundles-public.js', array( 'jquery' ), $this->version, true );
        wp_localize_script($this->plugin_name,
            'alpb',
            array('ajaxurl' => admin_url( 'admin-ajax.php' ) )
        );

	}

    /**
     * DEBUG
     */
	public function debug(){

	    $this->remove_old_temp_logos();
        $bundle = Al_Product_Bundles::get_bundles_session();
        if( empty( $bundle ) ) { ?>
            <p><?php _e('No bundles added', 'al-product-bundles'); ?></p>
            <?php
        }

        echo '<div class="container"><div class="row"><div class="col-md-12 p-2">';
        echo '<pre>';
        var_dump($_POST);
        echo '</pre>';
        echo '<form method="post" class="pull-right"><input type="hidden" name="al-purge-session" value="1"><button class="btn btn-danger"> Purge </button></form>';
        echo '<button type="button" class="btn btn-primary" data-toggle="modal" data-target="#al-product-bundles-modal">show Bundle</button>';

	    echo '</div></div></div>';
    }
    /**
     * Add bundle to session on form submit
     *
     */
    public function add_to_session() {
//        if( is_singular('product') ) {
            $bundle_id = $this->get_intval_post('al-add-bundle');
            $product_id = $this->get_intval_post('al-add-to-bundle');


            // add bundle to session
            if( $bundle_id !== false ) {
                // set bundle session
                $this->add_bundle_to_session( $bundle_id );

            // add product to bundle in session
            } elseif( $product_id !== false ) {
                $this->add_product_to_bundle_session($product_id);
            }
//        }
    }

    /**
     * Retrieves intval from $_POST with default value
     * @param $key
     * @param bool $default
     * @return bool|int
     */
    private function get_intval_post($key, $default = false) {

        $out = isset( $_POST[$key] ) && intval( $_POST[$key] ) !== 0 ? intval( $_POST[$key] ) : $default ;

        return $out;
    }

    /**
     * Retrieves boolean from $_POST with default value
     * @param $key
     * @return bool
     */
    private function get_boolean_post($key) {
        $out = isset( $_POST[$key] ) ? boolval( $_POST[$key] ) : false ;
        return $out;
    }

    /**
     * Adds bundle to session
     * @param $bundle_id
     */
    private function add_bundle_to_session($bundle_id) {
        $product = wc_get_product( $bundle_id );

        if( $product !== false && $product->is_type('yith_bundle' ) ) {
            $bundle_session = Al_Product_Bundles::get_bundles_session();
            if( ! empty( $bundle_session ) ) {
                wc_add_notice( self::err_mesg_bundle_cant_be_addd() , 'error');
            } else {

                $items_list = $product->get_bundled_items();
                $variations_selected = array();
                foreach ($items_list as $item) {
                    $variation_id = $this->get_intval_post('yith_bundle_variation_id_' . $item->item_id);
                    if ($variation_id !== false) {
                        $variations_selected[$item->item_id] = $variation_id;
                    }
                }

                Al_Product_Bundles::set_bundle_session( array( 'bundle_id' => $bundle_id, 'bundle_variations' => $variations_selected,'added_products' => array() ) );
                wc_add_notice( __('Pakket is toegevoegd', 'al-product-bundles') );
            }
        }
    }

    /**
     * Returns bundle standard products
     * @param $bundle_id
     * @param bool $only_ids
     * @return mixed
     */
    private function get_bundle_product_items($bundle_id, $only_ids = false) {
        $product = wc_get_product( $bundle_id );
        $items_list = $product->get_bundled_items();
        $out = $items_list;
        if( $only_ids ) {
            $out = array_keys($items_list);
        }

        return $out;
    }

    /**
     * Adds product to bundle in session
     * @param $product_id
     */
    private function add_product_to_bundle_session($product_id) {

        $print_logo_prod_id = $this->get_print_logo_product_id();

        if( $print_logo_prod_id == $product_id ){
            wc_add_notice( __('This product can\'t be added', 'al-product-bundles'), 'error');
            return;
        }

        $product = wc_get_product( $product_id );

        if( $product === false ) {
            return;
        }

        $bundle_session = Al_Product_Bundles::get_bundles_session();
        if( empty( $bundle_session ) ) {
            wc_add_notice( self::err_mesg_prod_cant_be_addd(), 'error');
            return;
        }


        if( $product->is_type('simple' ) ) {

            $qty = $this->get_intval_post('quantity', 1);
            $this->add_to_bundle($bundle_session, $product_id, $qty);
            self::print_product_adde_to_bundle_notice();
        }elseif ( $product->is_type('variable' ) ) {

            $variation_id = $this->get_intval_post('variation_id');

            // no variation ID passed
            if( $variation_id === false ) {
                return;
            }

            $variations_avail = $this->get_product_available_variation_ids($product_id);

            // this variation is not available or not related to this product
            if( empty( $variations_avail ) || !in_array( $variation_id, $variations_avail )) {
                return;
            }
            $qty = $this->get_intval_post('quantity', 1);
            $this->add_to_bundle($bundle_session, $product_id, $qty, $variation_id);
            self::print_product_adde_to_bundle_notice();
        }
    }


    /**
     * Add product to bundle in session
     * @param $bundle_session
     * @param $product_id
     */
    private function add_to_bundle($bundle_session, $product_id, $qty = 1, $variation_id = false) {

        $product_main = $variation_id !== false ? $variation_id : $product_id;

        // if product or variation are already in bundle
        if( key_exists( $product_main, $bundle_session['added_products']) ) {
            $bundle_session['added_products'][$product_main]['qty'] = $bundle_session['added_products'][$product_main]['qty'] + $qty;

        // new added product or bundle
        } else {
            $bundle_session['added_products'][$product_main]['qty'] = $qty;
            if( $variation_id !== false  ) {
                $bundle_session['added_products'][$product_main]['parent_prod'] = $product_id;
            }
        }
        Al_Product_Bundles::set_bundle_session($bundle_session);
    }



    /**
     * Returns available variation IDs for product
     * @param $product_id
     * @return array
     */
    private function get_product_available_variation_ids($product_id) {

        $args = array(
            'post_type'     => 'product_variation',
            'post_status'   => array( 'private', 'publish' ),
            'numberposts'   => -1,
            'orderby'       => 'menu_order',
            'order'         => 'asc',
            'post_parent'   => $product_id,
            'fields'        => 'ids'
        );
        $query = new WP_Query( $args );
        return $query->posts;
    }

    /**
     * Replace 'Add To Cart' template for product bundles
     * @param $located
     * @param $template_name
     * @param $args
     * @param $template_path
     * @param $default_path
     * @return string
     */
    public function replace_add_to_cart_templates($located, $template_name, $args, $template_path, $default_path) {

        if( $template_name == 'single-product/add-to-cart/yith-bundle.php' ) {
            $located = WP_PLUGIN_DIR . '/al-product-bundles/templates/yith-woocommerce-product-bundles/add-to-cart-bundle.php';
        }

        if( $template_name == 'single-product/add-to-cart/simple.php' ) {
            $located = WP_PLUGIN_DIR . '/al-product-bundles/templates/woocommerce/add-to-cart-simple.php';
        }

        if( $template_name == 'single-product/add-to-cart/variation-add-to-cart-button.php' ) {
            $located = WP_PLUGIN_DIR . '/al-product-bundles/templates/woocommerce/add-to-cart-variation.php';
        }

        return $located;
    }


    public function print_modal() {
        $bundle = Al_Product_Bundles::get_bundles_session();
        if( isset( $bundle['bundle_id'] ) ) {
            $bundle_items = $this->get_bundle_product_items($bundle['bundle_id'], true);
        }

        $is_all_standard_products_removed = ( isset( $bundle['standard_items_hidden'] ) && count( $bundle['standard_items_hidden'] ) === count($bundle_items ) ) && empty( $bundle['added_products'] ) ? true : false ;

            ?>
            <div class="container">
                <div class="modal woocommerce" id="al-product-bundles-modal" tabindex="-1" role="dialog">
                    <div class="modal-dialog" role="document">
                        <div class="modal-content">
                            <?php if( ! empty( $bundle ) ) { ?>
                                <div class="al-js-first-step al-first-step">
                                    <div class="modal-header">
                                        <h5 class="modal-title text-center"><?php echo get_the_title($bundle['bundle_id']); ?></h5>
                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                            <span aria-hidden="true">&times;</span>
                                        </button>
                                    </div>
                                    <div class="modal-body">
                                        <ul class="nav nav-tabs al-nav al-col-2">
                                            <li><a class="active" data-toggle="tab" href="#default"><?php _e('Producten in pakket', 'al-product-bundles'); ?></a></li>
                                            <li><a data-toggle="tab" href="#added"><?php _e('Extra gekozen producten', 'al-product-bundles'); ?></a></li>
                                        </ul>
                                        <div class="clearfix"></div>
                                        <div class="tab-content">
                                            <div id="default" class="tab-pane fade in active show">
                                                <?php $this->print_modal_products($bundle); ?>

                                            </div>
                                            <div id="added" class="tab-pane fade">
                                                <?php $this->print_modal_products($bundle, true); ?>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="modal-footer">
                                        <button type="button" class="btn btn-danger al-js-modal-second-step al-js-hidden-if-no-products" data-step="1" <?php echo $is_all_standard_products_removed ? 'style="display:none;"' : ''; ?>><?php _e('Ik ben klaar. Ga vstandard_items_hiddenerder', 'al-product-bundles'); ?></button>
                                        <button type="button" class="btn al-btn-white" data-dismiss="modal"><?php _e('Doorgaan met samenstellen', 'al-product-bundles'); ?></button>
                                    </div>
                                </div>

                                <div class="al-js-second-step al-second-step"></div>

                                <div class="al-loader al-js-loader">
                                    <div class="loader"></div>
                                </div>
                            <?php } ?>
                        </div>
                    </div>
                </div>
            </div>
        <?php
    }

    /**
     * Prints products in modal
     * @param $bundle
     * @param bool $added
     * @param bool $second_step
     */
    private function print_modal_products($bundle, $show_added = false, $second_step = false) {


        $items = $second_step === false ? $this->get_clean_product_data_array($bundle, $show_added) : $this->get_clean_product_data_array_grouped($bundle);

        if ( ! $second_step && empty( $items ) ) {
            $message = $show_added ? __('Er zijn nog geen extra producten toevoegd aan dit pakket', 'al-product-bundles') : __('Bundle doesn\'t contain products', 'al-product-bundles');
            ?>
            <p><?php echo $message; ?></p>
        <? } else {
            $hidden_items = array();
            if( ! $show_added ) {
                $hidden_items = isset( $bundle['standard_items_hidden'] ) && is_array( $bundle['standard_items_hidden'] ) ? $bundle['standard_items_hidden'] : array();
                $class = empty( $hidden_items ) ? 'al-hidden' : '';
                echo '<button class="al-js-restore-stand-prod ' . $class . '">' . __('Restore All Products') . '</button>';

            }
            ?>
            <ul class="al-col-2 al-product-list">
                <?php
                foreach ($items as $item) {
                    $hide_stand_item = isset($item['item_id']) && in_array($item['item_id'], $hidden_items);
                    $this->print_product_in_modal($item, $show_added, $second_step, $hide_stand_item);
                } ?>
            </ul>
            <div class="clearfix"></div>
            <?php
        }
    }


    /**
     * Prints line items in modal
     * @param $elem
     * @param bool $added
     * @param bool $sectond_step
     * @param bool $hide_stand_item
     */
    private function print_product_in_modal($elem, $added = false, $sectond_step = false, $hide_stand_item = false) {

        $class = $added ? 'al-is-added ' : 'al-standard-product ';
        $class .= $sectond_step ? 'al-js-mark ' : '';
        $class .= $hide_stand_item ? 'al-hidden ' : '';
        $span_qty = '<span>' . $elem['qty'] . ' x </span>';
        $span_attr = $elem['attributes'] !== '' ? ' - <span>' . $elem['attributes'] . '</span>' : '';
        ?>
        <li class="al-product al-rel <?php echo $class; ?>" data-product-id="<?php echo $elem['id']; ?>">
            <div class="al-mark">
                <?php

                $bundle = Al_Product_Bundles::get_bundles_session();
                Al_Product_Bundles::get_product_setting_remove_standard_product($bundle['bundle_id']);

                $standard_prods_not_removable = Al_Product_Bundles::get_product_setting_remove_standard_product($bundle['bundle_id']) === 'yes';

                if ( ! isset( $_POST['added_products'] ) ) {
                    $product_id = isset( $elem['item_id'] ) ? $elem['item_id'] : $elem['id'];
                    if( ! $standard_prods_not_removable || $added ) {
                        ?>
                        <button class="al-js-remove al-remove al-abs not-a-button" data-product-id="<?php echo $product_id; ?>" data-standard-product="<?php echo !$added ?>">x</button>
                        <?php
                    }
                }
                ?>
                <div class="prod-img float-left">
                    <img src="<?php echo $elem['img_url']; ?>" alt="<?php echo $elem['title']; ?>">
                </div>
                <div class="prod-info pull-left">
                    <h6><?php echo $span_qty; ?><span class="al-title"><?php echo $elem['title']; ?></span><?php echo $span_attr; ?></h6>
                </div>
                <div class="clearfix"></div>
            </div>
        </li>
        <?php
    }

    /**
     * Prepares clean product data, to show it in modal
     * @param $bundle
     * @param bool $added
     * @return array
     */
    private function get_clean_product_data_array($bundle, $added = false) {
        $out = array();
        if( $added ) {
            $bundle_items = isset( $bundle['added_products'] ) ? $bundle['added_products'] : false;
            // if there are products added  to bundle
            if( $bundle_items === false || empty( $bundle_items ) ) return $out;

            foreach ($bundle_items as $product_id => $product_data) {

                $product = wc_get_product($product_id);
                $thumbnail_url = $this->get_prod_thumbnail_url($product->get_image_id());

                $out[] = array(
                    'id'        => $product_id,
                    'img_url'    => $thumbnail_url,
                    'title'     =>$product->get_title(),
                    'qty'       => isset( $product_data['qty'] ) ? $product_data['qty'] : 1,
                    'attributes'=> implode(', ', wc_get_product_variation_attributes($product_id) )
                );
            }
        } else {
            $bundle_id = isset( $bundle['bundle_id'] ) ? $bundle['bundle_id'] : false;
            // if no bundle id in session
            if( $bundle_id == false ) return $out;

            $items_list = $this->get_bundle_product_items($bundle_id);

            $out = array();
            foreach ($items_list as $item_id => $item) {

                $variation_id = $variation_parent_id = false;
                $product = $item->product;

                if( isset( $bundle['bundle_variations'][$item->item_id] ) ) {
                    $variation_id = $bundle['bundle_variations'][$item->item_id];
                    $product = wc_get_product($variation_id);
                    $variation_parent_id = $item->product_id;
                }


                $min_qty         = $item->min_quantity;

                $title = $product->get_title();

                $thumbnail_url = $this->get_prod_thumbnail_url($product->get_image_id());

                $out[] = array(
                    'id'        => $variation_id !== false ? $variation_id : $item->product->get_id(),
                    'item_id'   => $item_id,
                    'img_url'   => $thumbnail_url,
                    'title'     => $title,
                    'qty'       => $min_qty,
                    'attributes'=> $variation_id !== false ? implode(', ', wc_get_product_variation_attributes($variation_id) ) : ''

                );

            }

        }
        return $out;
    }


    private function get_clean_product_data_array_grouped($bundle) {

        $out = $this->get_clean_product_data_array($bundle);
        $res = $this->get_clean_product_data_array($bundle, true);

        $out = array_merge($out, $res);

        $out_grouped = array();
        foreach($out as $key => $item){
            if( isset( $out_grouped[$item['id']] ) ) {
                $out_grouped[$item['id']]['qty'] = $out_grouped[$item['id']]['qty'] + $item['qty'];
            }else {
                $out_grouped[$item['id']] = $item;
            }
        }
        return $out_grouped;
    }


    /**
     * Retrieves thumbnail url
     * @param $attach_id
     * @return mixed
     */
    private function get_prod_thumbnail_url($attach_id) {

        $thumbnail = wp_get_attachment_image_src($attach_id, 'thumbnail');

        $thumbnail_url = $thumbnail[0] === null ? wc_placeholder_img_src() : $thumbnail[0];

        return $thumbnail_url;
    }

    /**
     * Prints error if user tries to add bundle to session, but there is another one already
     */
    public static function error_bundle_cant_be_added() {
        Al_Product_Bundles::print_error( self::err_mesg_bundle_cant_be_addd() , true);
    }

    /**
     * Prints error if user tries to add product, but there are no bundle in session
     */
    public static function error_product_cant_be_added_to_bundle() {
        Al_Product_Bundles::print_error( self::err_mesg_prod_cant_be_addd() , true);
    }

    /**
     *  Removes product from bundle
     */
    public function ajax_remove_product_from_session() {
        $product_id = $this->get_intval_post('product-id');
        $is_standard = $this->get_boolean_post('is-standard');
        
        if( $product_id === false ) {
            $this->ajax_response(array('status' => false, 'msg' => __('Product ID is not passed', 'al-product-bundles')));
        }

        $bundle = Al_Product_Bundles::get_bundles_session();

        if( empty( $bundle ) || !isset( $bundle['bundle_id'] ) ) {
            $this->ajax_response(array('status' => false, 'msg' => __('No bundle added', 'al-product-bundles')));
        }

        $flag_updated = false;
        if( $is_standard ) {

            if( Al_Product_Bundles::get_product_setting_remove_standard_product($bundle['bundle_id']) === 'yes' ) {
                $this->ajax_response(array('status' => false, 'msg' => __('You can\'t remove products from this bundle', 'al-product-bundles')));
            }

            $bundle_items = $this->get_bundle_product_items($bundle['bundle_id'], true);
            $amount_of_products_in_bundle = count( $bundle_items );
            if( in_array( $product_id,  $bundle_items ) ) {
                if( is_array( $bundle['standard_items_hidden'] ) && !in_array( $product_id, $bundle['standard_items_hidden'] )) {
                    $bundle['standard_items_hidden'][] = $product_id;
                    $flag_updated = true;
                } else {
                    $bundle['standard_items_hidden'] = array( $product_id );
                    $flag_updated = true;
                }
            }
        } elseif(  isset( $bundle['added_products'] ) && is_array( $bundle['added_products'] ) && isset( $bundle['added_products'][$product_id] )){
            unset($bundle['added_products'][$product_id]);
            $flag_updated = true;
        }
        // new value was set
        if( $flag_updated ) {
            Al_Product_Bundles::set_bundle_session($bundle);
            $bundle_content = Al_Product_Bundles::content_in_session_inner(false);
            $out = array('status' => true, 'msg' => __('Successfully removed', 'al-product-bundles'), 'bundle_content' => $bundle_content);

            if( ( isset( $amount_of_products_in_bundle ) &&  $amount_of_products_in_bundle === count($bundle['standard_items_hidden']) ) && empty( $bundle['added_products'] ) ) {
                $out['last_standard_product_removed'] = true;
            }
            $this->ajax_response($out);
        }
        $this->ajax_response(array('status' => false, 'msg' => __('No such product in the bundle', 'al-product-bundles')));
    }

    /**
     * Returns content of second step in modal
     */
    public function ajax_modal_step_body() {
        $bundle = Al_Product_Bundles::get_bundles_session();
        $step = isset( $_POST['step'] ) && intval($_POST['step']) > 0 ? intval($_POST['step']) : 1;

        if( empty( $bundle ) ) {
            $this->ajax_response( array('status' => false, 'msg' => __('Bundle hasn\'t been created yet', 'al-product-bundles')) );
        }
        ob_start();
        ?>
        <div class="modal-body">
            <?php
            if( $step > 1 ) {
                ?>
                <div class="modal-header">
                    <h5 class="modal-title text-center"><?php _e('Chose products for logo print', 'al-product-bundles'); ?></h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <p><?php _e('It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters.', 'al-product-bundles') ?></p>
                <?php
                $this->print_modal_products($bundle, true, true);
            }else{
                ?>
                <div class="modal-header">
                    <h5 class="modal-title text-center"><?php _e('Dit pakket is compleet', 'al-product-bundles') ?></h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <p><?php _e('It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters.', 'al-product-bundles') ?></p>
                <?php
            }
            ?>
        </div>
        <div class="modal-footer">

            <form method="POST" enctype="multipart/form-data">
                <?php
                $this->get_bundle_add_to_cart_fields($bundle);
                if( $step > 1 ) {
                    ?>
                    <div class="form-group upload-btn-wrapper">
                        <span class="btn"><?php _e('Select file','al-product-bundles') ?></span>
                        <input type="file" class="form-control-file" name="al-logo"  accept="image/*">
                        <span><?php _e('Logo uploaden','al-product-bundles') ?></span>
                    </div>
                    <div>
                        <button type="button" class="btn btn--secondary al-js-modal-second-step" data-step="1"><?php _e('Terug','al-product-bundles'); ?></button>
                        <button type="submit" class="btn btn-primary"><?php _e('Toevoegen aan winkelwagen','al-product-bundles'); ?></button>
                    </div>
                    <?php
                } else {
                    ?>
                    <div>
                    <button type="button" class="btn btn--secondary al-js-modal-second-step" data-step="0"><?php _e('Terug','al-product-bundles'); ?></button>
                    <button type="button" class="btn btn-danger al-js-modal-second-step" data-step="2"><?php _e('Logo toevoegen aan producten','al-product-bundles'); ?></button>
                    <button type="submit" class="btn btn--secondary"><?php _e('Ga door zonder logo print','al-product-bundles'); ?></button>
                    </div>
                    <?php
                }
                ?>
            </form>
        </div>
        <?php
        $out = ob_get_clean();
        $this->ajax_response( array('status' => true, 'body' => $out) );
    }


    public function ajax_alpb_get_modal_content() {


        ob_start();

        if( ! isset( $_POST['product_id'] ) ) {
            $out = array('status' => false, 'msg' => __('Niet alle gegevens zijn doorgegeven','al-product-bundles'));
        } else {
            $bundle_id = intval( $_POST['product_id'] );
            $added_products = isset( $_POST['added_products'] ) ? $_POST['added_products'] : array();
            ?>
            <div class="al-js-first-step al-first-step">
                <div class="modal-header">
                    <h5 class="modal-title text-center"><?php echo get_the_title($bundle_id); ?></h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <ul class="nav nav-tabs al-nav al-col-2">
                        <li><a class="active" data-toggle="tab" href="#default"><?php _e('Producten in pakket', 'al-product-bundles'); ?></a></li>
                        <li><a data-toggle="tab" href="#added"><?php _e('Extra gekozen producten', 'al-product-bundles'); ?></a></li>
                    </ul>
                    <div class="clearfix"></div>
                    <div class="tab-content">
                        <div id="default" class="tab-pane fade in active show">
                            <?php $this->print_modal_products(array( 'bundle_id' =>  $bundle_id)); ?>

                        </div>
                        <div id="added" class="tab-pane fade">
                            <?php $this->print_modal_products(array( 'bundle_id' =>  $bundle_id, 'added_products' => $added_products), true); ?>
                        </div>
                    </div>
                </div>
                <div class="modal-footer"></div>
            </div>
            <?php
            $body = ob_get_clean();


            $out = array('status' => true, 'body' => $body);
        }
        self::ajax_response($out);
    }

    /**
     * Prints fields needed to add bundle to cart
     * @param $bundle
     */
    private function get_bundle_add_to_cart_fields($bundle) {
        $bundle_id = $bundle['bundle_id'];
        $bundle_variations = $bundle['bundle_variations'];

        $items_list = $this->get_bundle_product_items($bundle_id);
        ?>
        <div class="form-group">
<!--            <input type="number" class="al-quantity" name="quantity" id="al-quantity" value="">-->
<!--            <label for="al-quantity">--><?php //_e('Aantal paketten','al-product-bundles'); ?><!--</label>-->
            <div class="qty mb-4 mt-3">
                <span class="al-js-set-qty al-minus bg-dark">-</span>
                <input type="number" class="count" name="quantity" id="al-quantity" value="10" min="10">
                <span class="al-js-set-qty al-plus bg-dark">+</span>

                <p class="mt-2"><small><i>Het minimale aantal betreft 10 pakketten</i></small></p>
            </div>
        </div>
        <input type="hidden" name="add-to-cart" value="<?php echo $bundle_id; ?>">
        <input type="hidden" class="al-js-logo-products" name="al-logo-products" value="">
        <input type="hidden"  name="al-add-to-cart" value="1">
        <?php
        foreach ($items_list as $item) {
            if (isset($bundle_variations[$item->item_id])) {
                $variation_id = $bundle_variations[$item->item_id];
                $product = wc_get_product($variation_id);
                $variation_attributes = $product->get_variation_attributes();
                foreach ($variation_attributes as $key => $val) {
                    ?>
                    <input type="hidden" name="yith_bundle_<?php echo $key; ?>_<?php echo $item->item_id; ?>" value="<?php echo $val; ?>">
                    <?php
                }

                if( $product->is_type('variation') ) { ?>
                    <input type="hidden" name="yith_bundle_variation_id_<?php echo $item->item_id; ?>" value="<?php echo $variation_id; ?>">
                <?php }
            }
            ?>
            <input type="hidden" name="yith_bundle_quantity_<?php echo $item->item_id; ?>" value="<?php echo $item->min_quantity; ?>">
            <?php
        }
    }


    /**
     * Add extra products when bundle is added to cart
     * @param $cart_item_key
     * @param $product_id
     * @param $quantity
     * @param $variation_id
     * @param $variation
     * @param $cart_item_data
     */
    public function woocommerce_add_to_cart( $cart_item_key, $product_id, $quantity, $variation_id, $variation, $cart_item_data ) {

        $product = wc_get_product( $product_id );

        if( $product->is_type( 'yith_bundle' ) ) {
            $this->add_to_cart_products($cart_item_key);
            Al_Product_Bundles::remove_bundle_session();
        }
        global $woocommerce;
        wp_safe_redirect($woocommerce->cart->get_cart_url());
    }

    /**
     * Adds bundle additional products to cart
     */
    public function add_to_cart_products($bciid) {
        if( ! isset( $_POST['al-add-to-cart'] ) ) return;

        $this->add_to_cart_print_logo_product($bciid);

        $bundle = Al_Product_Bundles::get_bundles_session();
        $added_products = isset( $bundle['added_products'] ) ? $bundle['added_products'] : false;

        if( $added_products === false ) return;

        $bundle_quantity = $this->get_intval_post('quantity', 1);

        foreach ($added_products as $product_id => $added_product) {

            $quantity = isset($added_product['qty']) ? $added_product['qty'] : 1;
            $quantity = $quantity * $bundle_quantity;
            $variation_id = 0;
            if( isset( $product_id[$product_id]['parent_prod'] ) ) {
                $variation_id = $product_id;
                $product_id = $product_id[$product_id]['parent_prod'];
            }

            WC()->cart->add_to_cart( $product_id, $quantity, $variation_id, false, array('bciid' => $bciid) );
        }

        Al_Product_Bundles::remove_bundle_session();
    }

    /**
     * Adds Print Logo products to cart
     * @throws Exception
     */
    private function add_to_cart_print_logo_product($bciid) {

        $print_logo_products = isset($_POST['al-logo-products']) ? $_POST['al-logo-products'] : '';
        $print_logo_file = isset( $_FILES['al-logo'] ) ? $_FILES['al-logo'] : '';
        // if no logo was uploaded or no products were selected to print logo on
        if( $print_logo_products === '' || $print_logo_file['name'] === '' ) return;

        $print_logo_prod_id = $this->get_print_logo_product_id();
        if( $print_logo_prod_id === false ) return;


        $print_logo_products_arr = json_decode($print_logo_products);
        if( ! is_array($print_logo_products_arr) || count($print_logo_products_arr) < 1 ) return;

        $bundle = Al_Product_Bundles::get_bundles_session();
        $bundles_products = $this->get_clean_product_data_array_grouped($bundle);
        $qty = $this->get_intval_post('quantity', 1);

        $total = 0;
        foreach ($bundles_products as $bundle_product) {
            if( in_array( $bundle_product['id'],  $print_logo_products_arr) ) {
                $total = $total + $bundle_product['qty'] * $qty;
            }
        }
        if( $total > 0 ) {
            if ( ! function_exists( 'wp_handle_upload' ) ) {
                require_once( ABSPATH . 'wp-admin/includes/file.php' );
            }
            add_filter( 'upload_dir', array($this, 'upload_dir_temp_folder') );
            $uploaded_file = wp_handle_upload( $print_logo_file, array('test_form' => false, 'unique_filename_callback' => array( $this, 'al_logo_upload_filter' ) ) );
            remove_filter( 'upload_dir', array($this, 'upload_dir_temp_folder') );

            $cart_item_data = array(
                'alpb_product_logo_print' => 1,
                'logo_url' => isset( $uploaded_file['url'] ) ? $uploaded_file['url'] : '',
                'logo_path' => isset( $uploaded_file['file'] ) ? $uploaded_file['file'] : '',
                'bundle_name' => get_the_title($bundle['bundle_id']),
                'bciid' => $bciid,
                'products_with_logo' => $print_logo_products_arr,
            );
            WC()->cart->add_to_cart( $print_logo_prod_id, $total, false, false, $cart_item_data );
        }
    }


    /**
     * Store cart item data, update order meta, move file to constant location
     * Link to uploaded file, as data of Print Logo product
     * @param $item
     * @param $cart_item_key
     * @param $values
     * @param $order
     */
    public function store_cart_item_data( $item, $cart_item_key, $values, $order ) {

        if ( isset( $values['alpb_product_logo_print'] ) ) {
            // move uploaded file to 'completed' folder
            $file_location  = $this->move_file_to_constant_folder( $values['logo_path'] );

            $item->update_meta_data( 'alpb_logo', array(
                'bciid' => $values['bciid'],
                'logo_url' => $file_location['url'],
                'logo_path' => $file_location['path'],
                'products_with_logo' => $values['products_with_logo'],
            ) );
        } elseif( isset( $values['bciid'] ) ){
            $item->update_meta_data( 'alpb_bciid', $values['bciid']);
        } else {
            $product_id = $item->get_product_id();
            $product = wc_get_product( $product_id );
            if( $product->is_type( 'yith_bundle' ) ) {
                $item->update_meta_data( 'alpb_bundle_hash', $cart_item_key);
            }
        }

        // save to cart item meta, added products
        if( isset( $values['alpb_added_products'] ) ) {
            $item->update_meta_data( 'alpb_added_products', $values['alpb_added_products']);
        }
        // save to cart item meta, standard items that was hidden
        if( isset( $values['alpb_standard_items_hidden'] ) ) {
            $item->update_meta_data( 'alpb_standard_items_hidden', $values['alpb_standard_items_hidden']);
        }
        // save to cart item meta, bundle selected product variations
        if( isset( $values['alpb_bundle_variations'] ) ) {
            $item->update_meta_data( 'alpb_bundle_variations', $values['alpb_bundle_variations']);
        }

        // save to cart item meta, bundle selected product variations
        if( isset( $values['bundled_item_id'] ) ) {
            $item->update_meta_data( 'alpb_bundled_item_id', $values['bundled_item_id']);
        }

    }

    /**
     * Moves files to constant location
     * @param $path
     * @return array
     */
    private function move_file_to_constant_folder($path) {

        $wp_upload = wp_upload_dir();

        $file_name = pathinfo($path, PATHINFO_BASENAME);
        $new_path = $wp_upload['basedir'] . '/alpb/const';
        $new_path_full = $new_path . '/' . $file_name;
        $new_url = $wp_upload['baseurl'] . '/alpb/const/' . $file_name;

        if ( ! file_exists($new_path ) ) {
            mkdir($new_path, 0755, true);
        }


        rename($path, $new_path_full);

        $out = array(
                'url' => $new_url,
                'path' => $new_path_full
        );
        return $out;
    }

    public function upload_dir_temp_folder( $dir ) {

        if ( ! file_exists($dir['basedir'] . '/alpb/temp' ) ) {
            mkdir($dir['basedir'] . '/alpb/temp', 0755, true);
        }

        return array(
                'path'   => $dir['basedir'] . '/alpb/temp',
                'url'    => $dir['baseurl'] . '/alpb/temp',
                'subdir' => '/alpb/temp',
            ) + $dir;
    }



    /**
     * Rename uploaded file name, its needed for auto delete old uploaded files, that wasn't moved to other folder
     * @param $dir
     * @param $name
     * @param $ext
     * @return string
     */
    public function al_logo_upload_filter( $dir, $name, $ext ) {
        return time().$ext;
    }

    /**
     * Returns ID of print logo product
     * @return bool
     */
    private function get_print_logo_product_id() {
        wp_reset_query();
        $args = array(
            'name'        => 'print-logo',
            'post_type'   => 'product',
            'post_status' => 'private',
            'numberposts' => 1
        );
        $out = get_posts($args);

        return isset( $out[0]->ID ) ? $out[0]->ID : false;
    }

    /**
     * JSON encode and die
     * @param $arr
     */
    private function ajax_response($arr) {
        echo json_encode($arr);
        die;
    }

    /**
     * Changes name of add to cart button
     * @param $text
     * @return string
     */
    public function add_to_cart_text($text) {
        global $product;

        if( $product->is_type('yith_bundle') || $product->is_type('simple') || $product->is_type('variable')) {
            $text = __('Add to Bundle', 'al-product-bundles');

            if( $product->is_type('yith_bundle') ) {
                $text = __('Use this Bundle', 'al-product-bundles');
            }
        }
        return $text;
    }

    /**
     * Purge bundle session
     */
    public function purge_session() {
        if ( isset( $_POST['al-purge-session'] ) ) {
            // purge session
            Al_Product_Bundles::remove_bundle_session();
        }
    }

    /**
     * Error message when product cant be added to session, as bundle hasn't been added yet
     * @return string
     */
    public static function err_mesg_prod_cant_be_addd() {
        return __('Je dient eerst een pakket te kiezen', 'al-product-bundles');
    }

    /**
     * Error message when 2nd bundle tries to be added
     * @return string
     */
    public static function err_mesg_bundle_cant_be_addd() {
        return __('Je kunt niet 2 pakketten tegelijk toevoegen. Maak het huidige pakket eerst af voordat je een nieuw pakket kiest.', 'al-product-bundles');
    }


    /**
     * Prints woo notice after product is added to bundle
     * @return string
     */
    public static function print_product_adde_to_bundle_notice() {
        wc_add_notice( __('Toegevoegd aan pakket', 'al-product-bundles') );
    }

    /**
     * Hide Print Logo Product from products page
     * @param $q
     * @return mixed
     */
    public function filter_product_query($q) {
        $print_logo_prod_id = $this->get_print_logo_product_id();
        if( $print_logo_prod_id !== false ) {
            $q->query_vars['post__not_in'][] = $print_logo_prod_id;
        }

        return $q;
    }

    /**
     * Remove old temp files, older than one week
     */
    private function remove_old_temp_logos() {
        $wp_upload = wp_upload_dir();
        $path_temp = $wp_upload['basedir'] . '/alpb/temp';

        if( file_exists( $path_temp ) ) {

            $files_in = scandir($path_temp);

            foreach ($files_in as $file) {
                // skip files
                if( in_array( $file, array('.', '..') ) ) continue;

                $filename = pathinfo($file, PATHINFO_FILENAME);
                if( $filename < strtotime("-1 week") ) {
                    unlink($path_temp . '/' . $file);
                }
            }
        }
    }


    /**
     * Updates order meta on after order is created
     * @param $order_id
     * @param $data
     */
    public function woocommerce_checkout_update_order_meta( $order_id, $data) {

        $order = wc_get_order( $order_id );

        $order_logo = $order_bundles = $order_bundle_added_products = $bundles_titles_counts = $k = $bundles_titles = $bundle_removed_standard_items = array();

        foreach ($order->get_items() as $item_key => $item_values) {
            // logo print product
            $alpb_logo = wc_get_order_item_meta($item_key, 'alpb_logo');
            if( $alpb_logo ) {
                $order_logo[$alpb_logo['bciid']]['logo_data'] = array(
                     'logo_url' => $alpb_logo['logo_url'],
                     'logo_path' => $alpb_logo['logo_path'],
                );

                $product_titles = array();
                foreach ($alpb_logo['products_with_logo'] as $products_with_logo) {
                    $product_titles[$products_with_logo] = get_the_title($products_with_logo);
                }

                $order_logo[$alpb_logo['bciid']]['products_with_logo'] = $product_titles;
            }
            // added product
            $alpb_bciid = wc_get_order_item_meta($item_key, 'alpb_bciid');
            if( $alpb_bciid !== '' ) {

                $product = $item_values->get_product();
                $order_bundle_added_products[$alpb_bciid][$item_values->get_product_id()] = $product->get_title();
            }

            $bundle_hash = wc_get_order_item_meta($item_key, 'alpb_bundle_hash');
            // bundle
            if( $bundle_hash !== '' ) {
                $order_bundles[$bundle_hash] = $item_values->get_product_id();

                $product = $item_values->get_product();
                $bundles_titles[$bundle_hash] = $product->get_title();

                $bundle_removed_standard_items[$bundle_hash] = wc_get_order_item_meta($item_key, 'alpb_standard_items_hidden');
            }
        }

        // if counter for same bundles in cart
        foreach ($order_bundles as $bciid => $bundle_id) {
            $k[$bundle_id] = isset( $k[$bundle_id] ) ? $k[$bundle_id] + 1 : 1;
            $bundles_titles_counts[$bciid] = $bundles_titles[$bciid] . ' - ' . $k[$bundle_id];


        }

        update_post_meta($order_id, 'alpb_logo', $order_logo);
        update_post_meta($order_id, 'alpb_added_products', $order_bundle_added_products);
        update_post_meta($order_id, 'alpb_removed_standard_items', $bundle_removed_standard_items);
        update_post_meta($order_id, 'alpb_bundles', $order_bundles);
        update_post_meta($order_id, 'alpb_bundles_counts', $bundles_titles_counts);
    }

    /**
     *  Creates unique cart keys - do add 2 bundles as separate items in cart
     * @param $cart_item_data
     * @param $product_id
     * @return mixed
     */
    public function woocommerce_add_cart_item_data( $cart_item_data, $product_id  ) {

        $cart_item_data['unique_key'] = uniqid();
        return $cart_item_data;
    }

    public function woocommerce_add_cart_item_data_filter( $cart_item_data, $product_id, $variation_id  ) {

        $product = wc_get_product( $product_id );

        if( $product !== false && $product->is_type('yith_bundle' ) ) {
            $bundle = Al_Product_Bundles::get_bundles_session();
            if( isset( $bundle['added_products'] ) ) {
                $cart_item_data['alpb_added_products'] = json_encode($bundle['added_products']);
            }

            if( isset( $bundle['standard_items_hidden'] ) && ! empty( $bundle['standard_items_hidden'] ) ) {
                $cart_item_data['alpb_standard_items_hidden'] = $bundle['standard_items_hidden'];
            }

            if( isset( $bundle['bundle_variations'] ) && ! empty( $bundle['bundle_variations'] ) ) {
                $cart_item_data['alpb_bundle_variations'] = json_encode($bundle['bundle_variations']);
            }
        }

        return $cart_item_data;
    }


     /**
     * Hides quantity box for added and logo print products
     * @param $product_quantity
     * @param $cart_item_key
     * @param $cart_item
     * @return mixed
     */
    public function woocommerce_cart_item_quantity($product_quantity, $cart_item_key, $cart_item) {

        if( isset( $cart_item['bciid'] ) || isset( $cart_item['alpb_logo'] ) ) {
            $product_quantity = $cart_item['quantity'];
        }

        return $product_quantity;
    }

    /**
     * Removes remove link on cart page
     * @param $link
     * @param $cart_item_key
     * @return string
     */
    public function woocommerce_cart_item_remove_link($link, $cart_item_key) {
        foreach (WC()->cart->get_cart() as $cart_key => $cart_item) {
            if( $cart_key === $cart_item_key ) {
                if( isset( $cart_item['bciid'] ) || isset( $cart_item['alpb_logo'] ) ) {
                    $link = '';
                }
            }
        }
        return $link;
    }

    /**
     * Hides products added to bundle
     * @param $class
     * @param $cart_item
     * @param $cart_item_key
     * @return string
     */
    public function woocommerce_cart_item_class( $class, $cart_item, $cart_item_key ) {

        foreach (WC()->cart->get_cart() as $cart_key => $cart_item_2) {
            // hide added products to bundle
            if( $cart_key === $cart_item_key ) {
                if( ( isset( $cart_item_2['bciid'] ) || isset( $cart_item_2['alpb_logo'] ) )
                    // needed to show logo-print product in the cart
                    && ! isset( $cart_item_2['logo_url'] )
                ) {
                    $class .= ' al-hidden';
                }
            }


            // hide removed standard product on Review Order page
            if( isset( $cart_item[ 'bundled_item_id' ] ) && isset( $cart_item_2['alpb_standard_items_hidden'] ) ) {
                if( !empty( $cart_item_2['alpb_standard_items_hidden'] ) && in_array($cart_item[ 'bundled_item_id' ], $cart_item_2['alpb_standard_items_hidden']) ) {
                    $class .= ' al-hidden';
                }
            }
        }
        return $class;
    }


    /**
     * Action when product is deleted, removes related to bundle products
     * @param $title
     * @param $cart_item
     * @return mixed
     */
    public function woocommerce_cart_item_removed_title($title, $cart_item) {

        $this->remove_related_products_with_bundle($cart_item['key']);

        return $title;
    }

    /**
     * @param $cart_item
     * @param $class
     */
    public function woocommerce_before_cart_item_quantity_zero($cart_item, $Class) {
        $this->remove_related_products_with_bundle($cart_item);
    }


    /**
     * Removes related to bundle products
     * @param $cart_item
     */
    private function remove_related_products_with_bundle($cart_item_key) {

        foreach (WC()->cart->get_cart() as $cart_key => $cart_item) {

            if( ( isset( $cart_item['bciid'] ) && $cart_item['bciid'] === $cart_item_key ) || ( isset( $cart_item['alpb_logo'] ) && isset( $cart_item['alpb_logo']['bciid'] ) && $cart_item['alpb_logo']['bciid'] === $cart_item_key) ) {
                WC()->cart->remove_cart_item( $cart_key );
            }
        }
    }

    /**
     * Updates quantity of related products
     * @param $cart_item_key
     * @param $quantity
     * @param $old_quantity
     * @param $Class
     */
    public function woocommerce_after_cart_item_quantity_update($cart_item_key, $quantity, $old_quantity, $Class ) {

        foreach (WC()->cart->get_cart() as $cart_key => $cart_item) {

            if( ( isset( $cart_item['bciid'] ) && $cart_item['bciid'] === $cart_item_key ) || ( isset( $cart_item['alpb_logo'] ) && isset( $cart_item['alpb_logo']['bciid'] ) && $cart_item['alpb_logo']['bciid'] === $cart_item_key) ) {

                $new_quantity = $cart_item['quantity'] / $old_quantity * $quantity;

                WC()->cart->set_quantity( $cart_key, $new_quantity, false );
            }
        }
    }

    /**
     * @param $cart_item
     * @param $cart_item_key
     */
    public function woocommerce_after_cart_item_name( $cart_item, $cart_item_key ) {
        if( isset( $cart_item['alpb_added_products'] ) ) {
            echo "<br><a class='al-js-modal-in-cart al-open-modal' href='' data-product-id='" . $cart_item['product_id'] . "' data-added-products='" . $cart_item['alpb_added_products'] . "'>" . __('Bekijk producten', 'al-product-bundles') . '</a>';
        }
    }

    /**
     *
     */
    public function woocommerce_add_to_cart_form_action($url) {
        return '';
    }

    /**
     * Updates cart item subtotal price
     * if its bundle with added extra products
     * @param $price
     * @param $cart_item
     * @return string
     */
    public function update_cart_subtotal_if_bundle_has_extra_prods($price, $cart_item ) {

        if( isset( $cart_item['alpb_added_products'] ) ) {
            $bundle_price = $this->get_cart_item_price_with_extra_products($cart_item);
            $bundle_qty = $cart_item['quantity'];
            if( $bundle_price !== false ) {
                $price = wc_price($bundle_price * $bundle_qty);
            }
        }
        return $price;
    }

    /**
     * Updates cart item price
     * if its bundle with added extra products
     * @param $price
     * @param $cart_item
     * @return string
     */
    public function update_cart_price_if_bundle_has_extra_prods($price, $cart_item ) {

        if( isset( $cart_item['alpb_added_products'] ) ) {
            $bundle_price = $this->get_cart_item_price_with_extra_products($cart_item);
            if( $bundle_price !== false ) {
                $price = wc_price($bundle_price );
            }
        }
        return $price;
    }

    /**
     * Returns bundle price in the cart, taking in consideration added/removed products
     * @param $cart_item
     * @return bool|float|int
     */
    private function get_cart_item_price_with_extra_products($cart_item) {
        if( ! isset( $cart_item['alpb_added_products'] ) ) {
            return false;
        }
        $price = false;
        $added_products = json_decode($cart_item['alpb_added_products']);
        if( ! empty($added_products) ) {
            $price = $cart_item['data']->get_price();
            foreach ($added_products as $added_product_id => $added_product_data ) {
                $qty = isset( $added_product_data->qty ) ? $added_product_data->qty : 1;
                if( isset( $added_product_data->parent_prod ) ) {
                    $product_variation = new WC_Product_Variation($added_product_id);
                    $extra_price = $product_variation->get_price();
                } else {
                    $product = new WC_Product($added_product_id);
                    $extra_price = $product->get_price();
                }

                $price += floatval($extra_price) * $qty;
            }
        }

        return $price;
    }

    /**
     * Restore removed standard products
     */
    public function ajax_restore_standard_products() {
        $bundle = Al_Product_Bundles::get_bundles_session();

        if( ! empty( $bundle ) && isset( $bundle['standard_items_hidden'] ) ) {
            unset( $bundle['standard_items_hidden'] );
            Al_Product_Bundles::set_bundle_session($bundle);
            $bundle_content = Al_Product_Bundles::content_in_session_inner(false);
            $this->ajax_response(array('status' => true, 'msg' => __('Restored', 'al-product-bundles'), 'bundle_content' => $bundle_content));
        }
        $this->ajax_response( array('status' => false, 'body' => __('No products were removed', 'al-product-bundles') ) );
    }

    /**
     * Updates bundle price if some standard products were removed
     * @param $cart
     */
    public function reduce_bundle_price_if_standard_prod_was_removed($cart) {
        if ( is_admin() && ! defined( 'DOING_AJAX' ) )
            return;

        if ( did_action( 'woocommerce_before_calculate_totals' ) >= 2 )
            return;

        foreach ( $cart->get_cart() as $cart_item ) {
            if( isset( $cart_item['alpb_standard_items_hidden'] ) && $cart_item['alpb_standard_items_hidden'] !== '' ) {

                $standard_items_hidden = $cart_item['alpb_standard_items_hidden'];

                // if no standard hidden items
                if( ! $standard_items_hidden ) {
                    return;
                }
                $bundle['standard_items_hidden'] = $standard_items_hidden;

                $bundle_selected_prod_variations = isset( $cart_item['alpb_bundle_variations'] ) && $cart_item['alpb_bundle_variations'] !== '' ? json_decode($cart_item['alpb_bundle_variations'], true) : false;

                if( $bundle_selected_prod_variations ) {
                    $bundle['bundle_variations'] = $bundle_selected_prod_variations;
                }

                $bundle['bundle_id'] = $cart_item['product_id'];

                $bundle_data = Al_Product_Bundles::get_bundle_price_and_total_products($bundle);

                $cart_item['data']->set_price( $bundle_data['total'] );
            }
        }
    }

    /**
     * Add Product Wrapping Fee
     */
    public function add_wrapping_fee() {
        global $woocommerce;
        $total_tax = 0;
        $items = $woocommerce->cart->get_cart();
        foreach($items as $item => $values) {
            // if its bundle main product or if its logo product
            if( isset( $values['alpb_added_products'] ) || isset( $values['alpb_product_logo_print'] )  ) {
                continue;
            }

            // if its standard product check if its not removed
            if( isset($values['bundled_by']) && isset( $values['bundled_item_id'] )) {
                if( $this->is_standard_product_removed_from_bundle($values['bundled_item_id'], $values['bundled_by']) ) {
                    continue;
                }
            }

            $total_tax += $this->wrapping_price * $values['quantity'];
        }

        if( $total_tax !== 0 ) {
            $woocommerce->cart->add_fee( __('Product Wrapping', 'woocommerce'), $total_tax );
        }
    }


    /**
     * Checks if standard product was removed from bundle
     * @param $cart_bundled_item_id
     * @param $cart_bundle_id
     * @return bool
     */
    private function is_standard_product_removed_from_bundle($cart_bundled_item_id, $cart_bundle_id) {

        foreach (WC()->cart->get_cart() as $cart_key => $cart_item) {
            $out = false;
            if( $cart_key === $cart_bundle_id && isset( $cart_item['alpb_standard_items_hidden'] ) && in_array( $cart_bundled_item_id, $cart_item['alpb_standard_items_hidden'] )) {
                return true;
            }
            return $out;
        }

    }
}
