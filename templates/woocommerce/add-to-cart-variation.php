<?php
/**
 * Single variation cart button
 *
 * @see https://docs.woocommerce.com/document/template-structure/
 * @package WooCommerce/Templates
 * @version 3.4.0
 */

defined( 'ABSPATH' ) || exit;

global $product;

$is_bundle_set = Al_Product_Bundles::is_bundle_set_in_session();
$disabled = $is_bundle_set ? '' : ' disabled="disabled" ';

?>
<div class="woocommerce-variation-add-to-cart variations_button">
    <?php do_action( 'woocommerce_before_add_to_cart_button' ); ?>

    <?php
    if( $is_bundle_set ) {
        do_action( 'woocommerce_before_add_to_cart_quantity' );

        woocommerce_quantity_input( array(
            'min_value'   => apply_filters( 'woocommerce_quantity_input_min', $product->get_min_purchase_quantity(), $product ),
            'max_value'   => apply_filters( 'woocommerce_quantity_input_max', $product->get_max_purchase_quantity(), $product ),
            'input_value' => isset( $_POST['quantity'] ) ? wc_stock_amount( wp_unslash( $_POST['quantity'] ) ) : $product->get_min_purchase_quantity(), // WPCS: CSRF ok, input var ok.
        ) );

        do_action( 'woocommerce_after_add_to_cart_quantity' );
        ?>
        <button type="submit" class="single_add_to_cart_button button alt" <?php echo $disabled; ?>><?php echo esc_html( $product->single_add_to_cart_text() ); ?></button>
        <?php
    } else {
        echo Al_Product_Bundles_Public::error_product_cant_be_added_to_bundle();
    }
    ?>
    <?php do_action( 'woocommerce_after_add_to_cart_button' ); ?>

    <input type="hidden" name="al-add-to-bundle" value="<?php echo absint( $product->get_id() ); ?>" />
    <input type="hidden" name="product_id" value="<?php echo absint( $product->get_id() ); ?>" />
    <input type="hidden" name="variation_id" class="variation_id" value="0" />
</div>
