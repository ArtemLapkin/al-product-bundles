<?php

/**
 * Fired during plugin deactivation
 *
 * @link       https://www.linkedin.com/in/artemlapkin/
 * @since      1.0.0
 *
 * @package    Al_Product_Bundles
 * @subpackage Al_Product_Bundles/includes
 */

/**
 * Fired during plugin deactivation.
 *
 * This class defines all code necessary to run during the plugin's deactivation.
 *
 * @since      1.0.0
 * @package    Al_Product_Bundles
 * @subpackage Al_Product_Bundles/includes
 * @author     Artem Lapkin <lapkin.artem@gmail.com>
 */
class Al_Product_Bundles_Deactivator {

	/**
	 * Short Description. (use period)
	 *
	 * Long Description.
	 *
	 * @since    1.0.0
	 */
	public static function deactivate() {

	}

}
