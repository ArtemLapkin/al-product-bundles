<?php

/**
 * The file that defines the core plugin class
 *
 * A class definition that includes attributes and functions used across both the
 * public-facing side of the site and the admin area.
 *
 * @link       https://www.linkedin.com/in/artemlapkin/
 * @since      1.0.0
 *
 * @package    Al_Product_Bundles
 * @subpackage Al_Product_Bundles/includes
 */

/**
 * The core plugin class.
 *
 * This is used to define internationalization, admin-specific hooks, and
 * public-facing site hooks.
 *
 * Also maintains the unique identifier of this plugin as well as the current
 * version of the plugin.
 *
 * @since      1.0.0
 * @package    Al_Product_Bundles
 * @subpackage Al_Product_Bundles/includes
 * @author     Artem Lapkin <lapkin.artem@gmail.com>
 */
class Al_Product_Bundles {

	/**
	 * The loader that's responsible for maintaining and registering all hooks that power
	 * the plugin.
	 *
	 * @since    1.0.0
	 * @access   protected
	 * @var      Al_Product_Bundles_Loader    $loader    Maintains and registers all hooks for the plugin.
	 */
	protected $loader;

	/**
	 * The unique identifier of this plugin.
	 *
	 * @since    1.0.0
	 * @access   protected
	 * @var      string    $plugin_name    The string used to uniquely identify this plugin.
	 */
	protected $plugin_name;

	/**
	 * The current version of the plugin.
	 *
	 * @since    1.0.0
	 * @access   protected
	 * @var      string    $version    The current version of the plugin.
	 */
	protected $version;

	/**
	 * Define the core functionality of the plugin.
	 *
	 * Set the plugin name and the plugin version that can be used throughout the plugin.
	 * Load the dependencies, define the locale, and set the hooks for the admin area and
	 * the public-facing side of the site.
	 *
	 * @since    1.0.0
	 */
	public function __construct() {
		if ( defined( 'AL_PRODUCT_BUNDLES' ) ) {
			$this->version = AL_PRODUCT_BUNDLES;
		} else {
			$this->version = '1.0.0';
		}
		$this->plugin_name = 'al-product-bundles';

		$this->load_dependencies();
		$this->set_locale();
		$this->define_admin_hooks();
		$this->define_public_hooks();

	}

	/**
	 * Load the required dependencies for this plugin.
	 *
	 * Include the following files that make up the plugin:
	 *
	 * - Al_Product_Bundles_Loader. Orchestrates the hooks of the plugin.
	 * - Al_Product_Bundles_i18n. Defines internationalization functionality.
	 * - Al_Product_Bundles_Admin. Defines all hooks for the admin area.
	 * - Al_Product_Bundles_Public. Defines all hooks for the public side of the site.
	 *
	 * Create an instance of the loader which will be used to register the hooks
	 * with WordPress.
	 *
	 * @since    1.0.0
	 * @access   private
	 */
	private function load_dependencies() {

		/**
		 * The class responsible for orchestrating the actions and filters of the
		 * core plugin.
		 */
		require_once plugin_dir_path( dirname( __FILE__ ) ) . 'includes/class-al-product-bundles-loader.php';

		/**
		 * The class responsible for defining internationalization functionality
		 * of the plugin.
		 */
		require_once plugin_dir_path( dirname( __FILE__ ) ) . 'includes/class-al-product-bundles-i18n.php';

		/**
		 * The class responsible for defining all actions that occur in the admin area.
		 */
		require_once plugin_dir_path( dirname( __FILE__ ) ) . 'admin/class-al-product-bundles-admin.php';

		/**
		 * The class responsible for defining all actions that occur in the public-facing
		 * side of the site.
		 */
		require_once plugin_dir_path( dirname( __FILE__ ) ) . 'public/class-al-product-bundles-public.php';

		$this->loader = new Al_Product_Bundles_Loader();

	}

	/**
	 * Define the locale for this plugin for internationalization.
	 *
	 * Uses the Al_Product_Bundles_i18n class in order to set the domain and to register the hook
	 * with WordPress.
	 *
	 * @since    1.0.0
	 * @access   private
	 */
	private function set_locale() {

		$plugin_i18n = new Al_Product_Bundles_i18n();

		$this->loader->add_action( 'plugins_loaded', $plugin_i18n, 'load_plugin_textdomain' );

	}

	/**
	 * Register all of the hooks related to the admin area functionality
	 * of the plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 */
	private function define_admin_hooks() {

		$plugin_admin = new Al_Product_Bundles_Admin( $this->get_plugin_name(), $this->get_version() );

		$this->loader->add_action( 'admin_enqueue_scripts', $plugin_admin, 'enqueue_styles' );
//		$this->loader->add_action( 'admin_enqueue_scripts', $plugin_admin, 'enqueue_scripts' );
        // prints order meta
		$this->loader->add_action('woocommerce_admin_order_data_after_shipping_address', $plugin_admin, 'print_order_meta');
		// Order detail page, order item print 'LOGO PRINT!'
        $this->loader->add_action('woocommerce_before_order_itemmeta', $plugin_admin, 'view_order_line_item_meta',10, 2);
        // Delete order action - remove uploaded logo
        $this->loader->add_action('before_delete_post', $plugin_admin, 'delete_order_hook');
        // Update item name, add bundle title with counter
        $this->loader->add_filter('woocommerce_order_item_get_name', $plugin_admin, 'update_product_title', 10, 2);
        // Add 'bundled' class to items related to bundle
        $this->loader->add_filter('woocommerce_admin_html_order_item_class', $plugin_admin, 'woocommerce_admin_html_order_item_class', 10, 2);
        // Hides not needed products meta on edit order page
        $this->loader->add_filter( 'woocommerce_order_item_get_formatted_meta_data', $plugin_admin, 'woocommerce_order_item_get_formatted_meta_data', 10, 2);
        // field on edit product page on Advanced Tab - Cant remove standard products
        $this->loader->add_action('woocommerce_product_options_advanced', $plugin_admin, 'field_setting_remove_standard_products');
        $this->loader->add_action('woocommerce_process_product_meta_yith_bundle', $plugin_admin, 'store_product_data');
	}

	/**
	 * Register all of the hooks related to the public-facing functionality
	 * of the plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 */
	private function define_public_hooks() {

		$plugin_public = new Al_Product_Bundles_Public( $this->get_plugin_name(), $this->get_version() );

		$this->loader->add_action( 'wp_enqueue_scripts', $plugin_public, 'enqueue_styles' );
		$this->loader->add_action( 'wp_enqueue_scripts', $plugin_public, 'enqueue_scripts' );
		// DEBUG
//		$this->loader->add_action( 'wp_head', $plugin_public, 'debug' );
		// Purge bundle session
		$this->loader->add_action( 'init', $plugin_public, 'purge_session' );
		// Replace add to cart templates
		$this->loader->add_filter( 'wc_get_template', $plugin_public, 'replace_add_to_cart_templates', 1000, 5 );
        // Add product to session
        $this->loader->add_action( 'template_redirect', $plugin_public, 'add_to_session' );
        // Ajax remove product from bundle
        $this->loader->add_action( 'wp_ajax_alpb_remove_from_bundle', $plugin_public, 'ajax_remove_product_from_session' );
        $this->loader->add_action( 'wp_ajax_nopriv_alpb_remove_from_bundle', $plugin_public, 'ajax_remove_product_from_session' );
        // Returns modal content for bundle popup on cart page
        $this->loader->add_action( 'wp_ajax_alpb_get_modal_content', $plugin_public, 'ajax_alpb_get_modal_content' );
        $this->loader->add_action( 'wp_ajax_nopriv_alpb_get_modal_content', $plugin_public, 'ajax_alpb_get_modal_content' );
        // Rename add to cart button
        $this->loader->add_filter( 'woocommerce_product_single_add_to_cart_text', $plugin_public, 'add_to_cart_text' );
        // Print Bundle modal
        $this->loader->add_action( 'wp_footer', $plugin_public, 'print_modal' );
        // Ajax modal 2nd step body
        $this->loader->add_action('wp_ajax_alpb_modal_step', $plugin_public, 'ajax_modal_step_body');
        $this->loader->add_action('wp_ajax_nopriv_alpb_modal_step', $plugin_public, 'ajax_modal_step_body');
        // Add to cart
        $this->loader->add_action('woocommerce_add_to_cart', $plugin_public, 'woocommerce_add_to_cart', 10, 6);
        // Hide print logo product
        $this->loader->add_filter('woocommerce_product_query', $plugin_public, 'filter_product_query');
        // Stores uploaded logo to const folder, updates order meta
        $this->loader->add_action('woocommerce_checkout_create_order_line_item', $plugin_public, 'store_cart_item_data', 10, 4 );
        // Update order meta
        $this->loader->add_action('woocommerce_checkout_update_order_meta', $plugin_public, 'woocommerce_checkout_update_order_meta', 10, 2 );
        // 2 same bundles in cart are separate items, unique cart_item_id
        $this->loader->add_action('woocommerce_add_cart_item_data', $plugin_public, 'woocommerce_add_cart_item_data', 10, 2 );
        // Update bundles data in cart, add list of added products
        $this->loader->add_filter('woocommerce_add_cart_item_data', $plugin_public, 'woocommerce_add_cart_item_data_filter', 10, 3 );
        // Removes quantity box on cart page
        $this->loader->add_filter('woocommerce_cart_item_quantity', $plugin_public, 'woocommerce_cart_item_quantity', 10, 3 );
        // Removes remove link on cart page
        $this->loader->add_filter('woocommerce_cart_item_remove_link', $plugin_public, 'woocommerce_cart_item_remove_link', 10, 2 );
        // add class to item in cart
        $this->loader->add_filter('woocommerce_cart_item_class', $plugin_public, 'woocommerce_cart_item_class', 10, 3 );
        // Remove Bundle with related products from cart
        $this->loader->add_filter('woocommerce_cart_item_removed_title', $plugin_public, 'woocommerce_cart_item_removed_title', 10, 2 );
        // Also removes bundle with related items, fires for update total cart, when quantity is set to 0
        $this->loader->add_action('woocommerce_before_cart_item_quantity_zero', $plugin_public, 'woocommerce_before_cart_item_quantity_zero', 10, 2 );
        // Updates quantity of related products in cart, when bundle quantity is changed
        $this->loader->add_action('woocommerce_after_cart_item_quantity_update', $plugin_public, 'woocommerce_after_cart_item_quantity_update', 10, 4 );
        // Add popup link for bundle on cart page
        $this->loader->add_action('woocommerce_after_cart_item_name',  $plugin_public, 'woocommerce_after_cart_item_name', 10, 2 );
        // Add to cart form action set to ''
        $this->loader->add_filter('woocommerce_add_to_cart_form_action', $plugin_public, 'woocommerce_add_to_cart_form_action');
        // cart page, cart item total price - bundle price with added products
        $this->loader->add_filter('woocommerce_cart_item_subtotal', $plugin_public, 'update_cart_subtotal_if_bundle_has_extra_prods', 10, 2);
        // cart page, cart item price - bundle price with added products
        $this->loader->add_filter('woocommerce_cart_item_price', $plugin_public, 'update_cart_price_if_bundle_has_extra_prods', 10, 2);
        // Restore removed standard products
        $this->loader->add_action( 'wp_ajax_alpb_restore_standard_products', $plugin_public, 'ajax_restore_standard_products' );
        $this->loader->add_action( 'wp_ajax_nopriv_alpb_restore_standard_products', $plugin_public, 'ajax_restore_standard_products' );
        // update bundle price when some standard products were removed
        $this->loader->add_action('woocommerce_before_calculate_totals', $plugin_public, 'reduce_bundle_price_if_standard_prod_was_removed');
        // add wrapping fee
//        $this->loader->add_action('woocommerce_cart_calculate_fees', $plugin_public, 'add_wrapping_fee');
    }

	/**
	 * Run the loader to execute all of the hooks with WordPress.
	 *
	 * @since    1.0.0
	 */
	public function run() {
		$this->loader->run();
	}

	/**
	 * The name of the plugin used to uniquely identify it within the context of
	 * WordPress and to define internationalization functionality.
	 *
	 * @since     1.0.0
	 * @return    string    The name of the plugin.
	 */
	public function get_plugin_name() {
		return $this->plugin_name;
	}

	/**
	 * The reference to the class that orchestrates the hooks with the plugin.
	 *
	 * @since     1.0.0
	 * @return    Al_Product_Bundles_Loader    Orchestrates the hooks of the plugin.
	 */
	public function get_loader() {
		return $this->loader;
	}

	/**
	 * Retrieve the version number of the plugin.
	 *
	 * @since     1.0.0
	 * @return    string    The version number of the plugin.
	 */
	public function get_version() {
		return $this->version;
	}

    /**
     * Get bundles from session
     * @return array|string
     */
    public static function get_bundles_session() {
        if( ! session_id() ) {
            session_start();
        }
        return isset( $_SESSION['al-bundles'] ) ? $_SESSION['al-bundles'] : array();
    }

    /**
     * Set bundles to session
     * @param $bundle
     */
    public static function set_bundle_session($bundle) {
        if( ! session_id() ) {
            session_start();
        }
        $_SESSION['al-bundles'] = $bundle;
    }

    /**
     * Set bundles to session
     */
    public static function remove_bundle_session() {
        if( ! session_id() ) {
            session_start();
        }
        unset( $_SESSION['al-bundles'] );
    }

    /**
     * Prints notice
     * @param $msg
     */
    public static function print_notice($msg) {
        if( $msg != '' ) {
            echo self::get_notice_body($msg);
        }
    }

    public static function is_bundle_set_in_session() {
        return ! empty(self::get_bundles_session());
    }

    /**
     * Prints error
     * @param $msg
     */
    public static function print_error($msg) {
        if( $msg != '' ) {
            echo self::get_notice_body($msg, true);
        }
    }

    /**
     * Returns tip body
     * @param $msg
     * @param bool $is_error
     * @return string
     */
    private static function get_notice_body($msg, $is_error = false) {
        $class = $is_error ? 'al-error' :'al-notice';
        $out = '<p class="al-tip ' . $class . '">' . $msg . '</p>';
        return $out;
    }


    /**
     * Prints Amount of standard bundle products and added ones and total
     */
    public static function print_content_in_session() {
        echo '<div class="al-bunlde-sidebar">';
        echo '<h4 class="color-red">' . __('Pakketmand', 'al-product-bundles') . '</h4>';
        echo '<div class="al-js-bundle-inner">';
        self::content_in_session_inner();
        echo '</div>';
        echo '</div>';
    }

    /**
     * Prints content of bundle in session, also used in remove product from bundle ajax
     */
    public static function content_in_session_inner($echo = true) {
        ob_start();
        if( self::is_bundle_set_in_session() ) {
            $bundle = self::get_bundles_session();

            $total = 0;

            if( isset( $bundle['bundle_id'] ) ) {

                $product = wc_get_product( $bundle['bundle_id'] );

                $bundle_data = self::get_bundle_price_and_total_products($bundle, $product);

                $total = $bundle_data['total'];
                $bundle_prod_count = $bundle_data['count'];

                echo '<h5>' . $product->get_title() . '</h5>';
                echo '<p>' . $product->get_description() . '</p>';
                if( 0 !== $bundle_prod_count ) {
                    echo '<p class="al-product-count al-bold"><span>' . $bundle_prod_count . '</span>' . _n('Standaard product', 'Standaard producten', $bundle_prod_count, 'al-product-bundles') . '</p>';
                }
            }

            $added_prod_count = isset( $bundle['added_products'] ) ? count( $bundle['added_products'] ) : 0;

            if( 0 !== $added_prod_count ) {
                echo '<p class="al-product-count al-bold"><span>' . $added_prod_count . '</span> ' . _n('Extra product', 'Extra producten', $added_prod_count, 'al-product-bundles') . '</p>';

                foreach ($bundle['added_products'] as $product_id => $data ) {
                    $qty = isset( $data['qty'] ) ? $data['qty'] : 1;
                    $product = wc_get_product($product_id);
                    $total +=  $product->get_price() * $qty;
                }
            }

            echo '<p class="al-product-count al-bold"><span>' . wc_price($total ) . '</span> ' . __('Totaal', 'al-product-bundles') . '</p>';
            echo '<button type="button" class="btn btn--primary d-block mt-2 w-100" data-toggle="modal" data-target="#al-product-bundles-modal">' . __('Bekijk inhoud', 'al-product-bundles') . '</button>';

        } else {
            echo '<p>' . __('Je winkelwagen is leeg. Kies een pakket om te beginnen', 'al-product-bundles') . '</p>';
            echo '<a href="'. site_url('/pc/pakketten') .'" class="btn btn--primary d-block mt-2 w-100">' . __('Pakket kiezen', 'al-product-bundles') . '</a>';
        }

        $out = ob_get_clean();

        if( $echo ) {
            echo $out;
        } else {
            return $out;
        }
    }

    /**
     * Returns Bundle calculated price and amount of standard products
     * @param $bundle
     * @param bool $bundle_product
     * @return array
     */
    public static function get_bundle_price_and_total_products( $bundle, $bundle_product = false ) {

        $product = $bundle_product && $bundle_product instanceof WC_Product ? $bundle_product : wc_get_product( $bundle['bundle_id'] );

        $items_list = $product->get_bundled_items();

        if( !isset( $bundle['standard_items_hidden'] ) || empty( $bundle['standard_items_hidden'] ) ) {
            $total = $product->get_price();
            $bundle_prod_count = count( $items_list );
        } else {
            $bundle_prod_count = $total = 0;
            foreach ($items_list as $item_id => $inner_product ){
                if( in_array($item_id, $bundle['standard_items_hidden']) ) continue;

                if( isset( $bundle['bundle_variations'][$item_id] ) ) {
                    $variation_id = $bundle['bundle_variations'][$item_id];
                    $inner_product = wc_get_product($variation_id);
                } else {
                    $inner_product = wc_get_product( $inner_product->product_id );
                }

                $total += floatval($inner_product->get_price());
                $bundle_prod_count++;
            }
        }
        $out = array(
            'total' => $total,
            'count' => $bundle_prod_count
        );
        return $out;
    }



    /**
     * Returns product 'al_cant_remove_standard_prods' meta
     * @param $prod_id
     * @return mixed
     */
    public static function get_product_setting_remove_standard_product($prod_id) {
        return get_post_meta($prod_id, 'al_cant_remove_standard_prods', true);
    }
}
