<?php

/**
 * Fired during plugin activation
 *
 * @link       https://www.linkedin.com/in/artemlapkin/
 * @since      1.0.0
 *
 * @package    Al_Product_Bundles
 * @subpackage Al_Product_Bundles/includes
 */

/**
 * Fired during plugin activation.
 *
 * This class defines all code necessary to run during the plugin's activation.
 *
 * @since      1.0.0
 * @package    Al_Product_Bundles
 * @subpackage Al_Product_Bundles/includes
 * @author     Artem Lapkin <lapkin.artem@gmail.com>
 */
class Al_Product_Bundles_Activator {

	/**
	 * Dependency on plugin activation
	 *
	 * Depends on WooCommerce and YITH WooCommerce Product Bundles Premium
	 *
	 * @since    1.0.0
	 */
	public static function activate() {

	    $error = '';

        if( ! is_plugin_active('yith-woocommerce-product-bundles-premium/init.php') ) {
            $error .= 'YITH WooCommerce Product Bundles Premium, ';
        }
        if( ! is_plugin_active('woocommerce/woocommerce.php') ) {
            $error .= 'WooCommerce, ';
        }

        if( $error !== '' ) {
            $error = rtrim($error, ', ');
            die( __( 'Al Product Bundles depends on ' . $error ) );
        }
    }
}
