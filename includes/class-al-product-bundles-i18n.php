<?php

/**
 * Define the internationalization functionality
 *
 * Loads and defines the internationalization files for this plugin
 * so that it is ready for translation.
 *
 * @link       https://www.linkedin.com/in/artemlapkin/
 * @since      1.0.0
 *
 * @package    Al_Product_Bundles
 * @subpackage Al_Product_Bundles/includes
 */

/**
 * Define the internationalization functionality.
 *
 * Loads and defines the internationalization files for this plugin
 * so that it is ready for translation.
 *
 * @since      1.0.0
 * @package    Al_Product_Bundles
 * @subpackage Al_Product_Bundles/includes
 * @author     Artem Lapkin <lapkin.artem@gmail.com>
 */
class Al_Product_Bundles_i18n {


	/**
	 * Load the plugin text domain for translation.
	 *
	 * @since    1.0.0
	 */
	public function load_plugin_textdomain() {

		load_plugin_textdomain(
			'al-product-bundles',
			false,
			dirname( dirname( plugin_basename( __FILE__ ) ) ) . '/languages/'
		);

	}



}
